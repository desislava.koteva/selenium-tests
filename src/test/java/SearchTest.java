import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SearchTest {

    public static final String URL_BING = "https://www.bing.com/";
    public static final String URL_GOOGLE = "https://www.google.com";
    public static final String SEARCH_TERM = "Telerik Academy Alpha";
    public static final String FIRST_RESULT_TEXT = "IT Career Start in 6 Months - Telerik Academy Alpha";

    public static final String MSG_SEARCH_INPUT = "Search input is not displayed";
    public static final String MSG_FIRST_LINK_NULL = "First link is null.";

    public static final String BING_INPUT_SEARCH_ID = "sb_form_q";
    public static final String BING_XPATH_FIRST_LINK = "//ol[@id='b_results']//div[1]//h2[1]//a";

    public static final String GOOGLE_COOCKIES_BTN_ID = "W0wltc";
    public static final String GOOGLE_XPATH_FIRST_LINK = "(//h3[@class='LC20lb MBeuO DKV0Md'])[1]";
    public static final String GOOGLE_XPATH_INPUT_SEARCH = "//input[@class='gLFyf gsfi']";

    public static final String CHROME_DRIVER_HOME = "C:\\chromedriver\\chromedriver.exe";

    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void beforeEachTest(){
        System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_HOME);
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
    }

    @Test
    public void searchBing() {
        driver.get(URL_BING);
        driver.manage().window().maximize();
        WebElement inputSearch = driver.findElement(By.id(BING_INPUT_SEARCH_ID));
        Assert.assertTrue(MSG_SEARCH_INPUT, inputSearch.isDisplayed());
        inputSearch.sendKeys(SEARCH_TERM);
        inputSearch.sendKeys(Keys.RETURN);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(BING_XPATH_FIRST_LINK)));
        WebElement firstLink = driver.findElement(By.xpath(BING_XPATH_FIRST_LINK));
        Assert.assertNotNull(MSG_FIRST_LINK_NULL, firstLink);
        Assert.assertEquals(FIRST_RESULT_TEXT, firstLink.getText());
    }

    @Test
    public void searchGoogle() {
        driver.get(URL_GOOGLE);
        driver.manage().window().maximize();
        driver.findElement(By.id(GOOGLE_COOCKIES_BTN_ID)).click();
        WebElement inputSearch = driver.findElement(By.xpath(GOOGLE_XPATH_INPUT_SEARCH));
        Assert.assertTrue(MSG_SEARCH_INPUT, inputSearch.isDisplayed());
        inputSearch.sendKeys(SEARCH_TERM);
        inputSearch.sendKeys(Keys.RETURN);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(GOOGLE_XPATH_FIRST_LINK)));
        WebElement firstLink = driver.findElement(By.xpath(GOOGLE_XPATH_FIRST_LINK));
        Assert.assertNotNull(MSG_FIRST_LINK_NULL, firstLink);
        Assert.assertEquals(FIRST_RESULT_TEXT, firstLink.getText());
    }

    @After
    public void afterEachTest(){
        driver.quit();
    }
}
